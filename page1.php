<?php
if (isset($_COOKIE['quiz'])) {
    $cookies = json_decode($_COOKIE['quiz'], true);
}
if (isset($_POST['next'])) {
    if (!isset($cookies)) {
        $cookies = $_POST;
    } else {
        $merge = array_merge($cookies, $_POST);
        $cookies = $merge;   
    }
    setcookie("quiz", json_encode($cookies), time() + 3600);
    header('location: page2.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trắc nghiệm online</title>
</head>

<body style="margin-left:50px; margin-top:10px">

    <form method="POST">
        <h1>
            Bài trắc nghiệm
        </h1>

        <?php
        $questions = array(
            "PHP viết tắt là gì?",
            "PHP được tạo ra năm bao nhiêu?",
            "Trình dịch PHP nào là trình dịch là đúng?",
            "Cài đặt Apache xong có thể kiểm tra servername qua cách gọi url http://localhost, cách khác là?",
            "PHP dựa theo syntax của ngôn ngữ nào?"
        );

        $answers = array(
            "Câu 1" => array('A' => "Phương Hay Phết", 'B' => "Pro Home Page", 'C' => "Personal Home Page", 'D' => "PHP"),
            "Câu 2" => array('A' => '1999', 'B' => '1994', 'C' => '1998', 'D' => '1995'),
            "Câu 3" => array('A' => "PHP Translator", 'B' => "PHP Interpreter", 'C' => "PHP Communicator", 'D' => "Không có câu nào đúng"),
            "Câu 4" => array('A' => "http://127.0.0.1", 'B' => "http://127.0.1.0", 'C' => "http://127.0.0.0", 'D' => "http://127.0.0.0"),
            "Câu 5" => array('A' => "Basic", 'B' => "Pascal", 'C' => "C", 'D' => "VB Script")
        );
        
        ?>

        <div>
            <?php
            $index = 1;
            foreach (array_keys($answers) as $question) {
                echo "<p>" . $question . ': ' . $questions[$index - 1] . "</p>";
                foreach (array_keys($answers[$question]) as $selection) {
                    echo "<input value='" . $selection . "' id='" . $question . $selection . "' type='radio' name='Q" . $index . "' style='margin-bottom: 30px'";
                    echo isset($cookies['Q' . $index]) && $cookies['Q' . $index] == $selection ? " checked" : "";
                    echo " /> ";
                    echo "<label for='" . $question . $selection . "'>" . $selection . '. ' . $answers[$question][$selection] . "</label><br>";
                }
                $index++;
            }
            ?>
        </div>
        <button type='submit' name='next'>Tiếp</button>
    </form>
</body>

</html>
